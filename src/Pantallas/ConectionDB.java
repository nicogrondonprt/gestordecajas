 package Pantallas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ConectionDB {
	public static String DBPath;
	public static String getDBPath() {
		return DBPath;
	}

	public static void setDBPath(String dBPath) {
		DBPath = dBPath;
	}

	public void insertarNuevo(String num_HC, String num_caja, String num_DNI) {

        // variables
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        // Step 1: Loading or registering Oracle JDBC driver class
        try {
 
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }
        catch(ClassNotFoundException cnfex) {
 
            System.out.println("Problem in loading or "
                    + "registering MS Access JDBC driver");
            cnfex.printStackTrace();
        }
	  
	        // Step 2: Opening database connection
	        try {
	 
	            String msAccDB = DBPath;
	            String dbURL =msAccDB; 
	 
	            // Step 2.A: Create and get connection using DriverManager class
	            connection = DriverManager.getConnection(dbURL); 
	 
	            // Step 2.B: Creating JDBC Statement 
	            statement = connection.createStatement();
	            // Step 2.C: Executing SQL &amp; retrieve data into ResultSet
	            /*resultSet =*/ statement.executeUpdate("INSERT INTO dbo.GestionDeCajas (num_hc, num_caja, num_dni,fecha) Values ('"+num_HC+"','"+num_caja+"','"+num_DNI+"',CURRENT_TIMESTAMP);");
	            resultSet = statement.executeQuery("Select * FROM dbo.GestionDeCajas");
	           // System.out.println("ID\\\tnumHC\t\t\tNumcaja\tDNI\tFecha");
	            //System.out.println("==\t================\t===\t=======");
	            // processing returned data and printing into console
	            System.out.println(resultSet.getRow());
	          /*  while(resultSet.next()) {
	                System.out.println(resultSet.getInt(1) + "\t" +
	                        resultSet.getString(2) + "\t" + // Historia Clinica
	                        resultSet.getString(3) + "\t" + // Numero De Caja
	                        resultSet.getString(4) + "\t" + // Numero de DNI
	                        resultSet.getString(5)); // Fecha de ingreso  
	            }*/
	        }
	        catch(SQLException sqlex){
	            sqlex.printStackTrace();
	        }
	        finally {
	 
	            // Step 3: Closing database connection
	            try {
	                if(null != connection) {
	 
	                    // cleanup resources, once after processing
	                    // resultSet.close();
	                    statement.close();
	 
	                    // and then finally close connection
	                    connection.close();
	                }
	            }
	            catch (SQLException sqlex) {
	                sqlex.printStackTrace();
	            }
	        }
	    
        
	}
	
	public void eliminar(int Id) {

        // variables
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        // Step 1: Loading or registering Oracle JDBC driver class
        try {
 
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }
        catch(ClassNotFoundException cnfex) {
 
            System.out.println("Problem in loading or "
                    + "registering MS Access JDBC driver");
            cnfex.printStackTrace();
        }
	  
	        // Step 2: Opening database connection
	        try {
	 
	            String msAccDB = DBPath;
	            String dbURL = msAccDB; 
	 
	            // Step 2.A: Create and get connection using DriverManager class
	            connection = DriverManager.getConnection(dbURL); 
	 
	            // Step 2.B: Creating JDBC Statement 
	            statement = connection.createStatement();
	            // Step 2.C: Executing SQL &amp; retrieve data into ResultSet
	            System.out.println(Id);
	            statement.executeUpdate("Delete FROM dbo.GestionDeCajas WHERE dbo.GestionDeCajas.ID="+Id+";");
	           // System.out.println("ID\\\tnumHC\t\t\tNumcaja\tDNI\tFecha");
	            //System.out.println("==\t================\t===\t=======");
	            // processing returned data and printing into console
	           
	        }
	        catch(SQLException sqlex){
	            sqlex.printStackTrace();
	        }
	        finally {
	 
	            // Step 3: Closing database connection
	            try {
	                if(null != connection) {
	 
	                    // cleanup resources, once after processing
	                    // resultSet.close();
	                    statement.close();
	 
	                    // and then finally close connection
	                    connection.close();
	                }
	            }
	            catch (SQLException sqlex) {
	                sqlex.printStackTrace();
	            }
	        }
	    
        
	}
	
	
	public String[][] devolverLista(TipoDeResultado tipo, String aux){
		String[][] HCS = null;

        // variables
        Connection connection = null;
        Statement statement = null;
        Statement statement2 = null;

        ResultSet resultSet = null;
        ResultSet resultSet1 = null;

        // Step 1: Loading or registering Oracle JDBC driver class
        try {
 
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            			//com.microsoft.sqlserver.jdbc.SQLServerDriver
        }
        catch(ClassNotFoundException cnfex) {
 
            System.out.println("Problem in loading or "
                    + "registering MS Access JDBC driver");
            cnfex.printStackTrace();
        }
	  
	        // Step 2: Opening database connection
	        try {
	 
	            String msAccDB = DBPath;
	            String dbURL =msAccDB; 
	 
	            // Step 2.A: Create and get connection using DriverManager class
	            connection = DriverManager.getConnection(dbURL); 
	 
	            // Step 2.B: Creating JDBC Statement 
	            statement = connection.createStatement();
	            statement2 = connection.createStatement();
	            // Step 2.C: Executing SQL &amp; retrieve data into ResultSet
	            switch(tipo) {
	            case completo:
		            resultSet =statement.executeQuery("Select * FROM dbo.GestionDeCajas");
		            resultSet1 = statement2.executeQuery("Select COUNT (*) FROM dbo.GestionDeCajas");
		            break;
	            case porDNI:
		            resultSet =statement.executeQuery("Select * FROM dbo.GestionDeCajas WHERE num_dni='"+aux+"'");
		            resultSet1 = statement2.executeQuery("Select COUNT (*) FROM dbo.GestionDeCajas WHERE num_dni='"+aux+"'");
		            
		            break;
	            case porHC:
		            resultSet =statement.executeQuery("Select * FROM dbo.GestionDeCajas WHERE num_hc='"+aux+"'");
		            resultSet1 = statement2.executeQuery("Select COUNT (*) FROM dbo.GestionDeCajas WHERE num_hc='"+aux+"'");
		            break;
	            case porCaja:
		            resultSet =statement.executeQuery("Select * FROM dbo.GestionDeCajas WHERE num_caja='"+aux+"'");
		            resultSet1 = statement2.executeQuery("Select COUNT (*) FROM dbo.GestionDeCajas WHERE num_caja='"+aux+"'");
		            break;
	           case porFecha:
	        	  // System.out.println("#"+StrToDate("2/25/2018")+"#");
		            resultSet =statement.executeQuery("Select * FROM dbo.GestionDeCajas WHERE fecha=#"+cambioFormato(aux)+"#;"); 
		            resultSet1 = statement2.executeQuery("Select COUNT (*) FROM dbo.GestionDeCajas WHERE fecha=#"+cambioFormato(aux)+"#;");
		            break;
		            
	            }

	            
	            // Get the number of rows from the result set
	            resultSet1.next();
	           
	            int rowcount = resultSet1.getInt(1);
	           System.out.println(rowcount);
	            // System.out.println("ID\\\tnumHC\t\t\tNumcaja\tDNI\tFecha");
	            //System.out.println("==\t================\t===\t=======");
	            // processing returned data and printing into console
	            
	           
	            HCS=new String[rowcount][5];
            	int i=0;
            	//System.out.println(HCS);
            	while(resultSet.next()){ 
            		//System.out.println("Nuevo Row");
            		//getString(2) en la DB (mdb formato) es el numero de CAJA
	            	HCS[i][0]=resultSet.getString(2);
	            	//getString(1) en la DB (mdb formato) es el numero de HC
	            	HCS[i][1]=resultSet.getString(1);
	            	//getString(3) en la DB (mdb formato) es el numero de DNI
	            	HCS[i][2]=resultSet.getString(3);
	            	String convertedDate="";
	            	//getString(4) en la DB (mdb formato) es la fecha
	            	//System.out.println(resultSet.getString(4));
	            	String dateReceivedFromUser = resultSet.getString(4).substring(0,10);;
	            	DateFormat userDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	            	DateFormat dateFormatNeeded = new SimpleDateFormat("dd/MM/yyyy");
	            	Date date;
					try {
						date = userDateFormat.parse(dateReceivedFromUser);
		            	convertedDate = dateFormatNeeded.format(date);

					} catch (ParseException e) {
							// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            	
	            	HCS[i][3]=convertedDate;//convertedDate
	            	//getString(5) en la DB (mdb formato) es el ID
	            	HCS[i][4]=String.valueOf(resultSet.getInt(5));
	            		i++;
	                		/*resultSet.getInt(1) + "\t" + //ID
	                        resultSet.getString(2) + "\t" + // Historia Clinica
	                        resultSet.getString(3) + "\t" + // Numero De Caja
	                        resultSet.getString(4) + "\t" + // Numero de DNI
	                        resultSet.getString(5)); // Fecha de ingreso  
	            */
	            }
	            

	            	

	        }
	        catch(SQLException sqlex){
	            sqlex.printStackTrace();
	        }
	        finally {
	 
	            // Step 3: Closing database connection
	            try {
	                if(null != connection) {
	 
	                    // cleanup resources, once after processing
	                    // resultSet.close();
	                    statement.close();
	 
	                    // and then finally close connection
	                    connection.close();
	                    
	                }
	            }
	            catch (SQLException sqlex) {
	                sqlex.printStackTrace();
	            }
	        }
	    
            return HCS;

	}
	
	public String cambioFormato(String fecha) {
		String convertedDate="";
    	//getString(4) en la DB (mdb formato) es la fecha
    	String dateReceivedFromUser = fecha;
    	DateFormat userDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    	DateFormat dateFormatNeeded = new SimpleDateFormat("yyyy-MM-dd");
    	Date date=null,date2;
		try {
			date2 = userDateFormat.parse(dateReceivedFromUser);
        	convertedDate = dateFormatNeeded.format(date2);
        	
		} catch (ParseException e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return convertedDate;

	}

	public String[][] traerRegistro(int id) {
		String[][] HCS = null;

        // variables
        Connection connection = null;
        Statement statement = null;
        Statement statement2 = null;

        ResultSet resultSet = null;
        ResultSet resultSet1 = null;

        // Step 1: Loading or registering Oracle JDBC driver class
        try {
 
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            			//com.microsoft.sqlserver.jdbc.SQLServerDriver
        }
        catch(ClassNotFoundException cnfex) {
 
            System.out.println("Problem in loading or "
                    + "registering MS Access JDBC driver");
            cnfex.printStackTrace();
        }
	  
	        // Step 2: Opening database connection
	        try {
	 
	            String msAccDB = DBPath;
	            String dbURL =msAccDB; 
	 
	            // Step 2.A: Create and get connection using DriverManager class
	            connection = DriverManager.getConnection(dbURL); 
	 
	            // Step 2.B: Creating JDBC Statement 
	            statement = connection.createStatement();
	            statement2 = connection.createStatement();
	            // Step 2.C: Executing SQL &amp; retrieve data into ResultSet

		            resultSet =statement.executeQuery("Select * FROM dbo.GestionDeCajas WHERE ID='"+id+"'");
		            resultSet1 = statement2.executeQuery("Select COUNT (*) FROM dbo.GestionDeCajas WHERE ID='"+id+"'");

	            // Get the number of rows from the result set
	             // System.out.println("ID\\\tnumHC\t\t\tNumcaja\tDNI\tFecha");
	            //System.out.println("==\t================\t===\t=======");
	            // processing returned data and printing into console
		            resultSet1.next();
			           
		            int rowcount = resultSet1.getInt(1);
		           System.out.println(rowcount);
		            // System.out.println("ID\\\tnumHC\t\t\tNumcaja\tDNI\tFecha");
		            //System.out.println("==\t================\t===\t=======");
		            // processing returned data and printing into console
		            
		           
		            HCS=new String[rowcount][5];
	            	int i=0;
	            	//System.out.println(HCS);
		        
            	//System.out.println(HCS);
            	while(resultSet.next()){ 
            		//System.out.println("Nuevo Row");
            		//getString(2) en la DB (mdb formato) es el numero de CAJA
	            	HCS[i][0]=resultSet.getString(2);
	            	//getString(1) en la DB (mdb formato) es el numero de HC
	            	HCS[i][1]=resultSet.getString(1);
	            	//getString(3) en la DB (mdb formato) es el numero de DNI
	            	HCS[i][2]=resultSet.getString(3);
	            	String convertedDate="";
	            	//getString(4) en la DB (mdb formato) es la fecha
	            	//System.out.println(resultSet.getString(4));
	            	String dateReceivedFromUser = resultSet.getString(4).substring(0,10);;
	            	DateFormat userDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	            	DateFormat dateFormatNeeded = new SimpleDateFormat("dd/MM/yyyy");
	            	Date date;
					try {
						date = userDateFormat.parse(dateReceivedFromUser);
		            	convertedDate = dateFormatNeeded.format(date);

					} catch (ParseException e) {
							// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            	
	            	HCS[i][3]=convertedDate;//convertedDate
	            	//getString(5) en la DB (mdb formato) es el ID
	            	HCS[i][4]=String.valueOf(resultSet.getInt(5));
	            		i++;
	                		/*resultSet.getInt(1) + "\t" + //ID
	                        resultSet.getString(2) + "\t" + // Historia Clinica
	                        resultSet.getString(3) + "\t" + // Numero De Caja
	                        resultSet.getString(4) + "\t" + // Numero de DNI
	                        resultSet.getString(5)); // Fecha de ingreso  
	            */
	            }
	            

	            	

	        }
	        catch(SQLException sqlex){
	            sqlex.printStackTrace();
	        }
	        finally {
	 
	            // Step 3: Closing database connection
	            try {
	                if(null != connection) {
	 
	                    // cleanup resources, once after processing
	                    // resultSet.close();
	                    statement.close();
	 
	                    // and then finally close connection
	                    connection.close();
	                    
	                }
	            }
	            catch (SQLException sqlex) {
	                sqlex.printStackTrace();
	            }
	        }
	    
            return HCS;
	}

	public void editarDatos(int id, String numeroDNI, String numeroHC, String caja) {
        // variables
        Connection connection = null;
        Statement statement = null;
        Statement statement2 = null;


        // Step 1: Loading or registering Oracle JDBC driver class
        try {
 
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            			//com.microsoft.sqlserver.jdbc.SQLServerDriver
        }
        catch(ClassNotFoundException cnfex) {
 
            System.out.println("Problem in loading or "
                    + "registering MS Access JDBC driver");
            cnfex.printStackTrace();
        }
	  
	        // Step 2: Opening database connection
	        try {
	 
	            String msAccDB = DBPath;
	            String dbURL =msAccDB; 
	 
	            // Step 2.A: Create and get connection using DriverManager class
	            connection = DriverManager.getConnection(dbURL); 
	 
	            // Step 2.B: Creating JDBC Statement 
	            statement = connection.createStatement();
	            statement2 = connection.createStatement();
	            // Step 2.C: Executing SQL &amp; retrieve data into ResultSet

		            statement.executeQuery("Update dbo.GestionDeCajas SET num_hc='"+numeroHC+"', num_caja='"+caja+"', num_dni='"+numeroDNI+"' WHERE ID='"+id+"'");
		         	            

	            	

	        }
	        catch(SQLException sqlex){
	            sqlex.printStackTrace();
	        }
	        finally {
	 
	            // Step 3: Closing database connection
	            try {
	                if(null != connection) {
	 
	                    // cleanup resources, once after processing
	                    // resultSet.close();
	                    statement.close();
	 
	                    // and then finally close connection
	                    connection.close();
	                    
	                }
	            }
	            catch (SQLException sqlex) {
	                sqlex.printStackTrace();
	            }
	        }
	    
	}
	
}