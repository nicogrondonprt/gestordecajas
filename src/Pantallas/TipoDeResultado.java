package Pantallas;

public enum TipoDeResultado {
	    completo(1),
	    porDNI(2),
	    porHC(3),
	    porCaja(4),
	    porFecha(5);
	    
	    private int i;

	    TipoDeResultado(int i) {
	        this.i = i;
	    }

	    public int Numerico() {
	        return i;
	    }
	}