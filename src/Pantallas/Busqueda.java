package Pantallas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import java.awt.Toolkit;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.ScrollPane;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class Busqueda extends JFrame {
	private ConectionDB conexion=new ConectionDB();
	ButtonGroup group = new ButtonGroup();
	private String[][] matrizActual;
	private JPanel contentPane;
	private JTextField busqueda;
	private JTable table;
	private JRadioButton rdbtnNumDeCaja;
	private JRadioButton rdbtnNumeroDeHc; 
	private JRadioButton rdbtnFecha;
	private JRadioButton rdbtnNumeroDNI;
	private DefaultTableModel model;
	private TipoDeResultado UltimoTipo;
	private String UltimoParametro;
	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 * @param id 
	 */
	public Busqueda() {
		
		setBackground(Color.WHITE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Busqueda.class.getResource("/Pantallas/Imagenes/ProintecLogo.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 800);

		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBackground(Color.WHITE);
		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		
		JButton btnNewButton = new JButton("Realizar Busqueda");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String parametro=busqueda.getText();
				if(rdbtnNumDeCaja.isSelected()) {
					tablaPorNumDeCaja(parametro);
				}
				if(rdbtnNumeroDeHc.isSelected()) {
					tablaPorNumDeHC(parametro);
				}
				if(rdbtnFecha.isSelected()) {
					tablaPorFecha(parametro);
				}
				if(rdbtnNumeroDNI.isSelected()) {
					tablaPorNumDNI(parametro);
				}
			}



			
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btnNewButton.setBounds(12, 511, 400, 70);
		contentPane.add(btnNewButton);
		
		JLabel lblBusquedaPor = new JLabel("Busqueda por :");
		lblBusquedaPor.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblBusquedaPor.setBounds(8, 165, 321, 34);
		contentPane.add(lblBusquedaPor);
		
		busqueda = new JTextField();
		busqueda.setFont(new Font("Tahoma", Font.PLAIN, 22));
		busqueda.setBounds(15, 450, 321, 25);
		contentPane.add(busqueda);
		busqueda.setColumns(10);
		

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Busqueda.class.getResource("/Pantallas/Imagenes/ProintecLogo.png")));
		lblNewLabel.setBounds(12, 13, 504, 145);
		contentPane.add(lblNewLabel);
		table = new JTable();
		Opciones();
	    JScrollPane scrollPane = new JScrollPane();
	    scrollPane.setBounds(515, 110, 424, 457);
		scrollPane.setToolTipText("");
	    contentPane.add(scrollPane);
	    table = new JTable(){
			// Para que no sea editable cada celda para el usuario
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		scrollPane.setViewportView(table);
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int Id,i;
				i=table.getSelectedRow();
				Id=Integer.parseInt(table.getModel().getValueAt(i, 4).toString());
				Editar editar = new Editar(Id,2);
				editar.setVisible(true);
				createTable(UltimoTipo,UltimoParametro);
				dispose();

			}
		});
		btnEditar.setBounds(842, 56, 97, 25);
		contentPane.add(btnEditar);
	}


	private void Opciones() {
		rdbtnNumDeCaja = new JRadioButton("Numero de Caja");
		rdbtnNumDeCaja.setToolTipText("");
		rdbtnNumDeCaja.setBackground(Color.WHITE);
		rdbtnNumDeCaja.setFont(new Font("Tahoma", Font.PLAIN, 22));
		rdbtnNumDeCaja.setBounds(15, 225, 291, 25);
		contentPane.add(rdbtnNumDeCaja);
		
		rdbtnNumeroDeHc = new JRadioButton("Numero de HC");
		rdbtnNumeroDeHc.setToolTipText("");
		rdbtnNumeroDeHc.setBackground(Color.WHITE);
		rdbtnNumeroDeHc.setFont(new Font("Tahoma", Font.PLAIN, 22));
		rdbtnNumeroDeHc.setBounds(15, 275, 231, 25);
		contentPane.add(rdbtnNumeroDeHc);
		
		rdbtnFecha = new JRadioButton("Fecha");
		rdbtnFecha.setToolTipText("");
		rdbtnFecha.setBackground(Color.WHITE);
		rdbtnFecha.setFont(new Font("Tahoma", Font.PLAIN, 22));
		rdbtnFecha.setBounds(15, 325, 127, 25);
		contentPane.add(rdbtnFecha);
		
		rdbtnNumeroDNI = new JRadioButton("Numero DNI");
		rdbtnNumeroDNI.setToolTipText("");
		rdbtnNumeroDNI.setBackground(Color.WHITE);
		rdbtnNumeroDNI.setFont(new Font("Tahoma", Font.PLAIN, 22));
		rdbtnNumeroDNI.setBounds(15, 375, 213, 25);
		contentPane.add(rdbtnNumeroDNI);
		
		    group.add(rdbtnNumDeCaja);
		    group.add(rdbtnNumeroDeHc);
		    group.add(rdbtnFecha);
		    group.add(rdbtnNumeroDNI);
		    
		    JButton btnNewButton_1 = new JButton("Exportar a CSV");
		    btnNewButton_1.addActionListener(new ActionListener() {
		    	public void actionPerformed(ActionEvent e) {
					CrearCSV c= new CrearCSV();
					JFileChooser fileChooser = new JFileChooser();
					fileChooser.setDialogTitle("Seleccione un directorio donde guardar");    

					int userSelection = fileChooser.showSaveDialog(contentPane);

					if (userSelection == JFileChooser.APPROVE_OPTION) {
					    File fileToSave = fileChooser.getSelectedFile();
					    
					    System.out.println("Save as file: " + fileToSave.getAbsolutePath());
					    c.crear(fileToSave,matrizActual);
					}
		    	}
		    });
		    btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 22));
		    btnNewButton_1.setBounds(12, 626, 400, 70);
		    contentPane.add(btnNewButton_1);
		    
		    JButton btnNewButton_2 = new JButton("Volver");
		    btnNewButton_2.addActionListener(new ActionListener() {
		    	public void actionPerformed(ActionEvent arg0) {
					Welcome welcome= new Welcome();
					welcome.setVisible(true);
					dispose();
		    	}
		    });
		    btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 22));
		    btnNewButton_2.setBounds(539, 626, 400, 70);
		    contentPane.add(btnNewButton_2);
		    
		    
		    
		    /*JScrollPane scrollPane = new JScrollPane();
		    scrollPane.setBounds(545, 26, 425, 582);
		    contentPane.add(scrollPane);
		    
		    table_2 = new JTable();
		    scrollPane.setViewportView(table_2);
		    */

		   

	}
	private void createTable(TipoDeResultado tipo,String parametro) {

		String titulos[]= {"Caja", "HC", "DNI", "Fecha","Id"};
		String informacion[][]=obtenerMatriz(tipo,parametro);
		matrizActual=informacion;
		table.setModel(new DefaultTableModel(informacion,titulos));
		table.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 18));
		table.setFont(new Font("Tahoma", Font.PLAIN, 18));
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TableColumnModel tcm= table.getColumnModel();
		tcm.removeColumn(table.getColumn("Id"));
		table.setModel(table.getModel());
		DefaultTableCellRenderer tcr= new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(tcr);
		table.getColumnModel().getColumn(1).setCellRenderer(tcr);
		table.getColumnModel().getColumn(2).setCellRenderer(tcr);
		table.getColumnModel().getColumn(3).setCellRenderer(tcr);
	}	
	private String[][] obtenerMatriz(TipoDeResultado tipo,String parametro) {
		String[][] HCS=conexion.devolverLista(tipo,parametro);
        UltimoTipo=tipo;
        UltimoParametro=parametro;
		return (HCS);
	}
	private void tablaPorNumDeCaja(String parametro) {
		// TODO Auto-generated method stub
		
		createTable(TipoDeResultado.porCaja,parametro);
	}
	private void tablaPorNumDNI(String parametro) {
		// TODO Auto-generated method stub
		
		createTable(TipoDeResultado.porDNI,parametro);


	}

	private void tablaPorFecha(String parametro) {
		// TODO Auto-generated method stub
		
		createTable(TipoDeResultado.porFecha,parametro);


	}

	private void tablaPorNumDeHC(String parametro) {
		// TODO Auto-generated method stub


		createTable(TipoDeResultado.porHC,parametro);
	}
}
