package Pantallas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import java.awt.Toolkit;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class Welcome extends JFrame {

	private JPanel contentPane;
	public Welcome() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Welcome.class.getResource("/Pantallas/Imagenes/ProintecLogo.png")));
		setTitle("Gestor de Cajas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 800);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.textHighlightText);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnBusqueda = new JButton("Busqueda");
		btnBusqueda.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent e) {
				Busqueda busqueda= new Busqueda();
				busqueda.setVisible(true);
				dispose();

			}
		});
		
		btnBusqueda.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnBusqueda.setBounds(12, 366, 450, 100);
		contentPane.add(btnBusqueda);
		
		JButton btnProcesarRegistro = new JButton("Procesar Registro");
		btnProcesarRegistro.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnProcesarRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Navigator navigator= new Navigator();
				navigator.setVisible(true);
				dispose();				
			}
		});
		btnProcesarRegistro.setBounds(520, 366, 450, 100);
		contentPane.add(btnProcesarRegistro);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Welcome.class.getResource("/Pantallas/Imagenes/ProintecLogo.png")));
		lblNewLabel.setBounds(236, -3, 734, 261);
		contentPane.add(lblNewLabel);
		
		JButton btnNuevaCaja = new JButton("Ingresar Nueva Caja");
		btnNuevaCaja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AgregarCaja agregarcaja= new AgregarCaja();
				agregarcaja.setVisible(true);
				dispose();
			}
		});
		btnNuevaCaja.setFont(new Font("Tahoma", Font.PLAIN, 35));
		btnNuevaCaja.setBounds(12, 510, 958, 150);
		contentPane.add(btnNuevaCaja);
		
		JLabel lblCajaActual = new JLabel("Caja Actual:  "+((Caja.getNumero()!=null)?Caja.getNumero():"No se a cargado ninguna caja"));
		lblCajaActual.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblCajaActual.setBounds(12, 246, 632, 52);
		contentPane.add(lblCajaActual);
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{contentPane}));
	}
}
