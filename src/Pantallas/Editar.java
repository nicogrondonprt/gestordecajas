package Pantallas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.awt.Toolkit;

public class Editar extends JFrame {

	private JPanel contentPane;
	private JTextField numHC;
	private ConectionDB conexion=new ConectionDB();
	private String [][] Registro;
	public Editar(int id, int i) {
		setTitle("Editar");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Editar.class.getResource("/Pantallas/Imagenes/ProintecLogo.png")));
		Registro= conexion.traerRegistro(id);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		numHC = new JTextField();
		numHC.setFont(new Font("Tahoma", Font.PLAIN, 16));
		numHC.setBounds(179, 36, 165, 22);
		numHC.setText(Registro[0][1]);
		contentPane.add(numHC);
		numHC.setColumns(10);
		
		JTextPane numDNI = new JTextPane();
		numDNI.setFont(new Font("Tahoma", Font.PLAIN, 16));
		numDNI.setBounds(179, 92, 165, 22);
		numDNI.setText(Registro[0][2]);
		contentPane.add(numDNI);
		
		JTextPane numcaja = new JTextPane();
		numcaja.setFont(new Font("Tahoma", Font.PLAIN, 16));
		numcaja.setBounds(177, 143, 167, 22);
		numcaja.setText(Registro[0][0]);
		contentPane.add(numcaja);
		
		JLabel lblNumeroDeHc = new JLabel("N\u00FAmero de HC");
		lblNumeroDeHc.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNumeroDeHc.setBounds(31, 39, 136, 16);
		contentPane.add(lblNumeroDeHc);
		
		JLabel lblNumeroDni = new JLabel("N\u00FAmero DNI");
		lblNumeroDni.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNumeroDni.setBounds(31, 92, 136, 16);
		contentPane.add(lblNumeroDni);
		
		JLabel lblNmeroDeCaja = new JLabel("N\u00FAmero de caja");
		lblNmeroDeCaja.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNmeroDeCaja.setBounds(31, 143, 136, 22);
		contentPane.add(lblNmeroDeCaja);
		
		JButton Editar = new JButton("Editar");
		Editar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		Editar.setBounds(12, 195, 165, 45);
		contentPane.add(Editar);
		
		JButton Cancelar = new JButton("Cancelar");
		Cancelar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		Cancelar.setBounds(255, 195, 165, 45);
		contentPane.add(Cancelar);
		
		
		Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Navigator navigator= new Navigator();
				navigator.setVisible(true);
				dispose();
				
			}
		});
			
			
		Editar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String DNItomado= numDNI.getText();
				String HCtomado= numHC.getText();
				HC hc=new HC(HCtomado,DNItomado,"");
				String caja=numcaja.getText();
				hc.setNumeroDNI(DNItomado);
				hc.setNumeroHC(HCtomado);	
				if(hc.nchValido()&& Caja.cajaValida(caja)) {
					conexion.editarDatos(id,hc.getNumeroDNI(),hc.getNumeroHC(),caja);
					JOptionPane.showMessageDialog(null, "Editado correctamente", "Editar", JOptionPane.INFORMATION_MESSAGE);
					if(i==1) {
						Navigator navigator= new Navigator();
						navigator.setVisible(true);
					}else {
						Busqueda busqueda=new Busqueda();
						busqueda.setVisible(true);
					}
					//navigator.setVisible(true);
					dispose();
				}else if(!hc.nchValido()) {
					JOptionPane.showMessageDialog(null, "Formato incorrecto de HC", "Formato incorrecto", JOptionPane.ERROR_MESSAGE);
				}else{
					JOptionPane.showMessageDialog(null, "Formato incorrecto de Caja", "Formato incorrecto", JOptionPane.ERROR_MESSAGE);
				}
			}	
		});
	}
}
