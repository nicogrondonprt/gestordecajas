package Pantallas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.ListSelectionModel;

public class Navigator extends JFrame {
	private ConectionDB conexion=new ConectionDB();

	private JPanel contentPane;
	private JTextField HC;
	private JTextField DNI;
	private JTable table;
	private DefaultTableModel model;


	public Navigator() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Navigator.class.getResource("/Pantallas/Imagenes/ProintecLogo.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 800);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
	//	listArticulos.add

		
		JButton agregarHC = new JButton("Agregar HC");
		agregarHC.setFont(new Font("Tahoma", Font.PLAIN, 18));
		agregarHC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String DNItomado= DNI.getText();
				String HCtomado= HC.getText();
				HC hc=new HC(HCtomado,DNItomado,"");

				hc.setNumeroDNI(DNItomado);
				hc.setNumeroHC(HCtomado);
				if(hc.nchValido()) {
					DNI.setVisible(false);
					String DNIVer=JOptionPane.showInputDialog(null, "Por favor reconfirme el DNI para ingresar la HC", "Ingreso de HC", JOptionPane.INFORMATION_MESSAGE);
					DNI.setVisible(true);

					if (DNIVer!=null&&DNIVer.equals(DNItomado)&&Caja.getNumero()!=null) {
						try {
							
							conexion.insertarNuevo(HCtomado,Caja.getNumero(), DNItomado);
							JOptionPane.showMessageDialog(null, "Se ha ingresado la HC", "Ingreso de HC", JOptionPane.INFORMATION_MESSAGE);
							HC.setText("");
							DNI.setText("");
							HC.requestFocusInWindow();
							//createTable();
							createTable();


						}
						catch(Error e) {
							JOptionPane.showMessageDialog(null, "Problema al conectar con DB", "Ingreso de HC", JOptionPane.ERROR_MESSAGE);

						}
						 
					
					}else {
						if(Caja.getNumero()==null) {
							JOptionPane.showMessageDialog(null, "Ingrese un numero de caja", "Ingreso de HC", JOptionPane.ERROR_MESSAGE);
						}else {
							JOptionPane.showMessageDialog(null, "Los DNI no coinciden", "Ingreso de HC", JOptionPane.ERROR_MESSAGE);
						}

						
					}
					
				}else {
			JOptionPane.showMessageDialog(null, "Formato erroneo de HC", "Ingreso de HC", JOptionPane.ERROR_MESSAGE);

				}

			}
		});
		agregarHC.setBounds(12, 50, 300, 50);
		contentPane.add(agregarHC);
		
		JButton EliminarHC = new JButton("Eliminar HC");
		EliminarHC.setFont(new Font("Tahoma", Font.PLAIN, 18));
		EliminarHC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogResult = JOptionPane.showConfirmDialog (null, "Esta seguro que desea eliminar?","Alerta",JOptionPane.OK_CANCEL_OPTION);
				if(dialogResult == JOptionPane.YES_OPTION){
				  // Saving code here
				
				int Id,i;
				i=table.getSelectedRow();
				Id=Integer.parseInt(table.getModel().getValueAt(i, 4).toString());
				conexion.eliminar(Id);
				createTable();
				}{
					
				}
				//show_user();
				//createTable();
			}
		});
		EliminarHC.setBounds(335, 50, 300, 50);
		contentPane.add(EliminarHC);
		
		JButton EditarHC = new JButton("Editar HC");
		EditarHC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int Id,i;
				i=table.getSelectedRow();
				Id=Integer.parseInt(table.getModel().getValueAt(i, 4).toString());
				Editar editar = new Editar(Id,1);
				editar.setVisible(true);
				createTable();
				dispose();
				
			}
		});
		EditarHC.setFont(new Font("Tahoma", Font.PLAIN, 18));
		EditarHC.setBounds(662, 50, 300, 50);
		contentPane.add(EditarHC);
		
		JLabel lblHc = new JLabel("HC");
		lblHc.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblHc.setVerticalAlignment(SwingConstants.TOP);
		lblHc.setBounds(510, 13, 285, 58);
		contentPane.add(lblHc);
		
		JLabel lblDni = new JLabel("DNI");
		lblDni.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblDni.setVerticalAlignment(SwingConstants.TOP);
		lblDni.setBounds(50, 13, 311, 58);
		contentPane.add(lblDni);
		
		HC = new JTextField();
		HC.setBounds(550, 10, 350, 28);
		contentPane.add(HC);
		HC.setColumns(10);
		DNI = new JTextField();
		DNI.setBounds(100, 10, 350, 28);
		contentPane.add(DNI);
		DNI.setColumns(10);
		HC.requestFocusInWindow();
		
		JButton Volver = new JButton("Volver");
		Volver.setFont(new Font("Tahoma", Font.PLAIN, 18));
		Volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Welcome welcome= new Welcome();
				welcome.setVisible(true);
				dispose();
			}
		});
		Volver.setBounds(512, 640, 450, 75);
		contentPane.add(Volver);
		this.getRootPane().setDefaultButton(agregarHC);
		JButton exportarCsv = new JButton("Exportar Csv");
		exportarCsv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CrearCSV c= new CrearCSV();
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Seleccione un directorio donde guardar");    

				int userSelection = fileChooser.showSaveDialog(contentPane);

				if (userSelection == JFileChooser.APPROVE_OPTION) {
				    File fileToSave = fileChooser.getSelectedFile();
				    
				    System.out.println("Save as file: " + fileToSave.getAbsolutePath());
				    c.crear(fileToSave,obtenerMatriz());
				}

				
				
				
			}
		});
		exportarCsv.setFont(new Font("Tahoma", Font.PLAIN, 18));
		exportarCsv.setBounds(12, 640, 450, 75);
		contentPane.add(exportarCsv);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setToolTipText("");
		scrollPane.setBounds(12, 115, 950, 500);
		contentPane.add(scrollPane);
		table = new JTable(){
			// Para que no sea editable cada celda para el usuario
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		scrollPane.setViewportView(table);
		createTable();

	/*	table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"Caja", "HC", "DNI", "Fecha"
			}
		));*/
		
		

	}
	
	private void createTable() {
		//this.repaint();
		
		String titulos[]= {"Caja", "HC", "DNI", "Fecha","Id"};
		String informacion[][]=obtenerMatriz();
		table.setModel(new DefaultTableModel(informacion,titulos));

		table.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 18));
		table.setFont(new Font("Tahoma", Font.PLAIN, 18));
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TableColumnModel tcm= table.getColumnModel();
		tcm.removeColumn(table.getColumn("Id"));
		table.setModel(table.getModel());
		DefaultTableCellRenderer tcr= new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(tcr);
		table.getColumnModel().getColumn(1).setCellRenderer(tcr);
		table.getColumnModel().getColumn(2).setCellRenderer(tcr);
		table.getColumnModel().getColumn(3).setCellRenderer(tcr);

	}

	private String[][] obtenerMatriz() {
		String[][] HCS=conexion.devolverLista(TipoDeResultado.completo,null);
        
		return (HCS);
	}
}
