package Pantallas;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CrearCSV {

	public void crear(File name,String[][] datos) {
		final String nombreDeArchivo = name+".csv";
		crearArchivoCSV(name,datos);
	}

	private static void crearArchivoCSV(File nombreDeArchivo,String[][] datos) {
		// this also can be "\t" for tab delimitador
		crearArchivoCSV(nombreDeArchivo, ";",datos);
	}

	private static void crearArchivoCSV(File file, String delim,String[][] datos) {
		final String NEXT_LINE = "\n";
		try {
			//file.createNewFile();
			FileWriter fw = new FileWriter(file.getAbsolutePath()+".csv");
			for(int x = 0; x < datos.length; x++) { 
				for (int y = 0; y < 4; y++) { 
					fw.append(datos[x][y]).append(delim);
					}
				fw.append(NEXT_LINE);
				}
			/*fw.append("testing").append(delim);
			fw.append("123").append(NEXT_LINE);

			fw.append("value1");
			fw.append(delim);
			fw.append("312");
			fw.append(NEXT_LINE);

			fw.append("anotherthing,888\n");
			*/
			fw.flush();
			fw.close();
		} catch (IOException e) {
			// Error al crear el archivo, por ejemplo, el archivo 
			// est� actualmente abierto.
			e.printStackTrace();
		}
	}
}