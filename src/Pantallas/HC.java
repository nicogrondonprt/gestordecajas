package Pantallas;


import java.util.ArrayList;
import java.util.Date;

public class HC {
	private String numeroHC;
	private String numeroDNI;
	private String Fecha;
	private String numeroCaja;
	
	public HC(String _HC, String _dni, String _fecha) {
		this.numeroHC = _HC;
		this.numeroDNI = _dni;
		this.Fecha = _fecha;
		this.numeroCaja = Caja.getNumero();
	}
	
	public String getNumeroHC() {
		return numeroHC;
	}
	public void setNumeroHC(String numeroHC) {
		numeroHC = numeroHC;
	}
	public String getNumeroDNI() {
		return numeroDNI;
	}
	public void setNumeroDNI(String numeroDNI) {
		numeroDNI = numeroDNI;
	}
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
	public String getNumeroCaja() {
		return numeroCaja;
	}
	public void setNumeroCaja(String numeroCaja) {
		numeroCaja = numeroCaja;
	}
	/*public boolean dniValido() {
		int dni;
		boolean resultado;
	        try {
	            dni=Integer.parseInt(this.NumeroDNI);
	            if(dni>1000000&&dni<99999999) {
	            	resultado = true;	
	            }else {
	            	resultado = false;
	            }
	        } catch (NumberFormatException excepcion) {
	            resultado = false;
	        }

		
		return resultado;
	}*/
	public boolean nchValido() {
		boolean valido=false;
		if(this.numeroHC.length()==13) {
			valido=true;
		}
		// TODO Auto-generated method stub
		return valido;
	}

}
