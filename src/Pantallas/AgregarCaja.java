package Pantallas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AgregarCaja extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField nroCaja;
	private JButton testing;


	/**
	 * Create the frame.
	 */
	
	public AgregarCaja() {
		ConectionDB conection= new ConectionDB();

		setIconImage(Toolkit.getDefaultToolkit().getImage(AgregarCaja.class.getResource("/Pantallas/Imagenes/ProintecLogo.png")));
		setTitle("Introducir Caja");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 800);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		/*
		testing = new JButton("testing");
		testing.addActionListener(this);
		testing.setFont(new Font("Tahoma", Font.PLAIN, 30));
		testing.setBounds(12, 12, 450, 100);
		contentPane.add(testing);
		*/
		nroCaja = new JTextField();
		nroCaja.setForeground(new Color(0, 0, 0));
		nroCaja.setFont(new Font("Tahoma", Font.PLAIN, 28));
		nroCaja.setBounds(12, 271, 500, 40);
		contentPane.add(nroCaja);
		nroCaja.setColumns(10);
		
		JLabel lblNroCaja = new JLabel("Numero de Caja a ingresar");
		lblNroCaja.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblNroCaja.setBounds(12, 188, 437, 103);
		contentPane.add(lblNroCaja);
		
		
		
		
		JButton btnIngresarCaja = new JButton("Ingresar");
		btnIngresarCaja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			String numeroDeCajaNuevo= nroCaja.getText();
			
			if(Caja.cajaValida(numeroDeCajaNuevo)) {
				Caja.setNumero(numeroDeCajaNuevo);
				JOptionPane.showMessageDialog(null, "La Caja "+numeroDeCajaNuevo+" se a ingresado satisfactoriamente y esta disponible para su uso", "Caja Guardada", JOptionPane.INFORMATION_MESSAGE);
				Welcome welcome= new Welcome();
				welcome.setVisible(true);
				dispose();
			}
				else {
					JOptionPane.showMessageDialog(null, "Formato de Numero de caja incorrecto", "Ingreso de Caja", JOptionPane.ERROR_MESSAGE);

				}
			//}
			
			}
		});
		btnIngresarCaja.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnIngresarCaja.setBounds(12, 366, 450, 100);
		contentPane.add(btnIngresarCaja);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Welcome welcome= new Welcome();
				welcome.setVisible(true);
				dispose();
			}
		});
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnCancelar.setBounds(520, 366, 450, 100);
		contentPane.add(btnCancelar);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(AgregarCaja.class.getResource("/Pantallas/Imagenes/Caja.jpg")));
		lblNewLabel_1.setBounds(349, 504, 553, 216);
		contentPane.add(lblNewLabel_1);
		
		this.getRootPane().setDefaultButton(btnIngresarCaja);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(AgregarCaja.class.getResource("/Pantallas/Imagenes/ProintecLogo.png")));
		label.setBounds(236, -3, 734, 261);
		contentPane.add(label);

	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource().equals(testing))
		{
			JOptionPane.showMessageDialog(null, "xdxdxdxd", "Ingreso de Caja", JOptionPane.ERROR_MESSAGE);
		}
	}
}
